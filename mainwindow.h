#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>

#include "serialreader.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow: public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);

  ~MainWindow();

public slots:
  void  processSerialData(QByteArray data);

private slots:
  void  on_pbConnect_toggled(bool checked);

  void  on_tbRefresh_clicked();

  void  on_pbResetCounter_clicked();

private:
  Ui::MainWindow *ui;

  // Serial reader
  SerialReader       *mSerialReader = nullptr;
  QByteArray          _mBuffer;
  quint64             mCounter   = 0;
  quint64             mFindError = 0;
  QStandardItemModel *model;
};
#endif // MAINWINDOW_H
