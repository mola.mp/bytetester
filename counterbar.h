#ifndef COUNTERBAR_H
#define COUNTERBAR_H

#include <QWidget>
#include <QList>
#include <QElapsedTimer>

class CounterBar: public QWidget
{
  Q_OBJECT

public:
  explicit CounterBar(QWidget *parent = nullptr);

  void  clear();

  void  setCounter(int index);

signals:
  // QWidget interface

protected:
  void  paintEvent(QPaintEvent *event);

private:
  int            mCounter[255];
  int            mCurrentIndex = 0;
  QElapsedTimer  mElapsedTimer;
};

#endif // COUNTERBAR_H
