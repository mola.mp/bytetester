#ifndef SERIALREADER_H
#define SERIALREADER_H

#include <QByteArray>
#include <QThread>

class QSerialPort;
class SerialReader: public QThread
{
  Q_OBJECT

public:
  SerialReader(QObject *parent = nullptr);

  const QString & portName() const;

  void            setPortName(const QString &newPortName);

public slots:
  void            write(QByteArray data);

signals:
  void            serialdata(QByteArray);

private slots:
  void            readyRead();

  // QThread interface

protected:
  void  run();

private:
  QSerialPort *m_serialPort;
  QString      mPortName;
  QByteArray   _buffer;
};

#endif // SERIALREADER_H
