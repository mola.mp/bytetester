#ifndef PACKET_H
#define PACKET_H

#include <cstdint>

struct PacketReciverHeader
{
  uint32_t  header : 24;
  uint8_t   headerType;
  uint16_t  size;
} __attribute__((packed));

struct PacketReciverRSSI
{
  uint32_t  header : 24;
  uint8_t   headerType;
  uint16_t  size;
  uint32_t  rssi : 18;
} __attribute__((packed));

struct PacketReciverGPS
{
  uint32_t  header : 24;
  uint8_t   headerType;
  uint16_t  size;
  char     *gps;
} __attribute__((packed));

struct PacketReciverNorthFinder
{
  uint32_t  header : 24;
  uint8_t   headerType;
  uint16_t  size;
  int16_t   x;
  int16_t   z;
  int16_t   y;
} __attribute__((packed));

struct PacketReciverStatus
{
  uint32_t  header : 24;
  uint8_t   headerType;
  uint16_t  size;
  uint32_t  ip;
  uint16_t  port;
  uint32_t  freq;
  uint8_t   rate;
} __attribute__((packed));

struct PacketConfig
{
  uint64_t  header : 40;
  uint8_t   headerType;
  uint32_t  ip;
  uint32_t  port;
  uint16_t  freq_lo_1;
  uint16_t  freq_lo_2;
  uint8_t   power;
  uint8_t   rate;
} __attribute__((packed));

struct PacketConfigUp
{
  uint8_t   header;
  uint8_t   packetId      : 4;
  uint8_t   packetVersion : 4;
  uint8_t   dataLenght;
  uint8_t   counter;
  uint8_t   uavId;
  uint8_t   payload[13];
  uint16_t  checksum;
} __attribute__((packed));

struct PacketConfigUp_01
{
  uint8_t  speed;
  uint8_t  height;
  uint8_t  flightMode1;
  uint8_t  flightMode2[9];
  uint8_t  reserve;
} __attribute__((packed));

struct PacketConfigUp_02
{
  uint8_t  Roll;
  uint8_t  Current;
  uint8_t  Batt;
  uint8_t  joySmallRight;
  uint8_t  joySmallLeft;
  uint8_t  joySmallUp;
  uint8_t  joySmallDown;
  uint8_t  joyBigY1;
  uint8_t  joyBigX1;
  uint8_t  joyBigY2;
  uint8_t  joyBigX2;
  bool     switchDown      : 1;
  bool     switchUp        : 1;
  bool     switchCorner_02 : 1;
  bool     switchCorner_01 : 1;
  bool     switchSmall_02  : 1;
  bool     switchSmall_01  : 1;
  bool     switchBig_02    : 1;
  bool     switchBig_01    : 1;
  uint8_t  reserve;
} __attribute__((packed));

struct PacketConfigUp_03
{
  uint8_t  pointActivity : 4;
  uint8_t  pointLabel    : 4;
  uint8_t  flightMode;
  int32_t  lat;// (Deg*100000)
  int32_t  lng;// (Deg*100000)
  int16_t  height;// (m*5)
  uint8_t  reserve;
} __attribute__((packed));

struct PacketConfigUp_03_reply
{
  uint8_t  packetId      : 4;
  uint8_t  packetVersion : 4;
  uint8_t  dataLenght;
  uint8_t  uavId;
  uint8_t  pointActivity : 4;
  uint8_t  pointLabel    : 4;
  uint8_t  flightMode;
  int32_t  lat;// (Deg*100000)
  int32_t  lng;// (Deg*100000)
  int16_t  height;// (m*5)
  uint8_t  reserve;
} __attribute__((packed));

struct PacketConfigUp_04
{
  uint32_t  ip;
  uint16_t  port;
  uint32_t  freq;
  uint8_t   power;
  uint8_t   rate;
  uint8_t   reserve;
} __attribute__((packed));

struct PacketConfigUp_04_reply
{
  uint8_t   packetId      : 4;
  uint8_t   packetVersion : 4;
  uint8_t   dataLenght;
  uint8_t   uavId;
  uint32_t  ip;
  uint16_t  port;
  uint32_t  freq;
  uint8_t   power;
  uint8_t   rate;
  uint8_t   reserve;
} __attribute__((packed));

struct PacketConfigUp_05
{
  uint32_t  freq;
} __attribute__((packed));

struct PacketConfigUp_06
{
  uint8_t  uavid;
} __attribute__((packed));


struct PacketConfigUp_06_reply
{
  uint8_t  packetId      : 4;
  uint8_t  packetVersion : 4;
  uint8_t  dataLenght;
  uint8_t  uavId;
} __attribute__((packed));

struct PacketConfigUp_09
{
  uint32_t  number;
} __attribute__((packed));

struct PacketConfigUp_09_reply
{
  uint8_t   packetId      : 4;
  uint8_t   packetVersion : 4;
  uint8_t   dataLenght;
  uint8_t   uavId;
  uint32_t  number;
} __attribute__((packed));

struct PayloadStruct
{
  uint16_t  header;
  uint8_t   ProjectID;
  uint8_t   PacketID;
  uint8_t   numberOfPayload;
  uint32_t  Time;
  int16_t   GPS_Velocity;
  int16_t   Air_Speed;
  int16_t   HeightOfGPS;
  int16_t   H_local; // m : 0.20
  int16_t   Motor_Voltage;
  uint8_t   Joy_ap_THT;
  int16_t   VCC;
  uint8_t   autopilot_status1;
  uint8_t   autopilot_status2;
  int16_t   Ax_16488; // 40/32767
  int16_t   Ay_16488; // 40/32767
  int16_t   Az_16488; // 40/32767
  int16_t   p_16488;  // 35/32767
  int16_t   q_16488;  // 35/32767
  int16_t   r_16488;  // 35/32767
  int8_t    GPS_SatNum;
  uint8_t   GPS_Quality;
  int8_t    FixType;
  uint32_t  Latitude;
  uint32_t  Longitude;
  int32_t   GPS_X;
  int32_t   GPS_Y;
  int16_t   GPS_Z;
  int16_t   Garmin_VN;
  int16_t   Garmin_VE;
  int16_t   Garmin_VD;
  int16_t   Roll_angle;
  int16_t   Pitch_angle;
  int16_t   yaw_angle;
  int16_t   FusionRoll_angle;
  int16_t   FusionPitch_angle;
  int16_t   Fusionyaw_angle;
  int32_t   X;
  int32_t   Y;
  int16_t   Z;
  int16_t   FusionVelocityOf_N;
  int16_t   FusionVelocityOf_E;
  int16_t   FusionVelocityOf_D;
  int16_t   HeightofBody_Pressure;
  uint8_t   Prog_ver;
  uint8_t   Logic;
  int16_t   Roll_cmd;
  int16_t   Pitch_cmd;
  int16_t   Yaw_cmd;
  int16_t   Acmd_y;
  int16_t   Acmd_z;
  uint8_t   Seeker_Status;
  int16_t   seeker_yaw_angle;
  int16_t   seeker_pitch_angle;
  int16_t   seeker_yaw_rate;
  int16_t   seeker_pitch_rate;
  int16_t   seeker_yaw_eps;
  int16_t   seeker_pitch_eps;
  int32_t   X_ins;
  int32_t   Y_ins;
  int16_t   Z_ins;
  int16_t   VX_ins;
  int16_t   VY_ins;
  int16_t   VZ_ins;
  int16_t   FOV_x;
  uint8_t   reserve_01[10];
  uint8_t   payload[16];
  int16_t   checksum;
} __attribute__((packed));
#endif // PACKET_H
