#include "serialreader.h"
#include <QSerialPortInfo>
#include <QSerialPort>
#include <iostream>

#include "packet.h"

SerialReader::SerialReader(QObject *parent):
  QThread(nullptr)
{
  setObjectName("SerialReader");
  moveToThread(this);
}

void  SerialReader::readyRead()
{
  QByteArray  data = m_serialPort->readAll();

  Q_EMIT  serialdata(data);
}

void  SerialReader::run()
{
  m_serialPort = new QSerialPort();
  m_serialPort->setPortName(mPortName);
  m_serialPort->setBaudRate(QSerialPort::Baud38400);
  m_serialPort->setDataBits(QSerialPort::Data8);
  m_serialPort->setFlowControl(QSerialPort::NoFlowControl);
  m_serialPort->setParity(QSerialPort::NoParity);
  m_serialPort->setStopBits(QSerialPort::OneStop);
  m_serialPort->open(QIODevice::ReadWrite);

  if (m_serialPort->open(QIODevice::ReadWrite))
  {
    std::cout << "Device open at : " << mPortName.toStdString() << std::endl;
    m_serialPort->setBreakEnabled(false);
  }
  else
  {
    std::cout << "Device not open at : " << mPortName.toStdString() << std::endl;
  }

  m_serialPort->setBreakEnabled(false);

  connect(m_serialPort, &QSerialPort::readyRead, this, &SerialReader::readyRead, Qt::QueuedConnection);
  exec();

  m_serialPort->close();
  delete m_serialPort;
  m_serialPort = nullptr;
  std::cout << " Quit ..." << std::endl;
}

const QString& SerialReader::portName() const
{
  return mPortName;
}

void  SerialReader::setPortName(const QString &newPortName)
{
  mPortName = newPortName;
}

void  SerialReader::write(QByteArray data)
{
  if (m_serialPort->isWritable())
  {
    m_serialPort->write(data);
  }
}
