#include "counterbar.h"

#include <QPaintEvent>
#include <QPainter>

CounterBar::CounterBar(QWidget *parent):
  QWidget(parent)
{
  mElapsedTimer.start();
  clear();
}

void  CounterBar::clear()
{
  mCurrentIndex = 0;
  memset(mCounter, 0x0, sizeof(int) * 255);
}

void  CounterBar::setCounter(int index)
{
  mCounter[index] = index;
  mCurrentIndex   = index;

  if (mElapsedTimer.elapsed() > 30)
  {
    update();
  }
}

void  CounterBar::paintEvent(QPaintEvent *)
{
  QPainter  qp(this);
  int       w    = width();
  int       h    = height();
  int       step = (int)qRound((double)h / 255);

  qp.setPen(Qt::darkBlue);
  qp.setBrush(Qt::darkBlue);
  qp.drawRect(0, 0, w, h);

  qp.setPen(Qt::yellow);
  qp.setBrush(Qt::yellow);

  int  steper = h - step;

  for (int i = 0; i < mCurrentIndex; i++)
  {
    if (mCounter[i] > 0)
    {
      qp.drawRect(0, steper, w, step);
    }

    steper -= step;
  }

  mElapsedTimer.restart();
}
