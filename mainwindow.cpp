#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>

#include <QSerialPortInfo>
#include "packet.h"

MainWindow::MainWindow(QWidget *parent):
  QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  on_tbRefresh_clicked();


  model = new QStandardItemModel;
  ui->listView->setModel(model);
  // TEST
  mSerialReader = new SerialReader(nullptr);
  connect(mSerialReader, &SerialReader::serialdata, this, &MainWindow::processSerialData, Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void  MainWindow::on_pbConnect_toggled(bool checked)
{
  if (checked)
  {
    ui->pbConnect->setText("Close");
    mSerialReader->setPortName(ui->comboDevices->currentText());
    mSerialReader->start();
  }
  else
  {
    ui->pbConnect->setText("Connect");
    mSerialReader->quit();
    mSerialReader->wait();
  }
}

void  MainWindow::on_tbRefresh_clicked()
{
  auto  devices = QSerialPortInfo::availablePorts();

  ui->comboDevices->clear();

  for (auto d : devices)
  {
    ui->comboDevices->addItem(d.portName());
  }
}

void  MainWindow::processSerialData(QByteArray data)
{
  for (int i = 1; i < data.size(); i++)
  {
    quint8  pd = data[i - 1];
    quint8  d  = data[i];

    if (d - pd > 1)
    {
      mFindError++;
      model->appendRow(new QStandardItem(QString("%1 -> %2").arg(pd).arg(d)));
    }
  }

  mCounter += data.size();
  ui->lblCounter->setText(QString::number(mCounter));
  ui->lblFindError->setText(QString::number(mFindError));
}

void  MainWindow::on_pbResetCounter_clicked()
{
  mCounter   = 0;
  mFindError = 0;
  model->clear();
}
